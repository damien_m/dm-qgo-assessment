export const ADD_ITEM = 'qgo/assessment/ADD_ITEM';
export const REMOVE_ITEM = 'qgo/assessment/REMOVE_ITEM';
export const TOGGLE_ITEM = 'qgo/assessment/TOGGLE_ITEM';
export const SET_FILTER = 'qgo/assessment/SET_FILTER';

export const COMPLETED = 'qgp/assessment/COMPLETED';
export const IN_PROGRESS = 'qgp/assessment/IN_PROGRESS';
export const SHOW_ALL = 'qgp/assessment/SHOW_ALL';

export const addItem = content => ({
 type: ADD_ITEM, 
 content
});

export const removeItem = id => ({
  type: REMOVE_ITEM,
  id
});

export const toggleItem = id => ({
  type: TOGGLE_ITEM,
  id
});

export const setItemFilter = itemFilter => ({
  type: SET_FILTER,
  itemFilter
});

