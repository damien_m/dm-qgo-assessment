import { createSelector } from 'reselect';
import {
  COMPLETED,
  IN_PROGRESS,
  SHOW_ALL,
} from '../actions/todos';

const getItemFilter = state => state.itemFilter;
const getItems = state => state.items

export const getVisibleItems = createSelector(
  [getItemFilter, getItems],
  (filter, items) => {
    switch(filter) {
      case SHOW_ALL:
        return items;
      case COMPLETED:
        return items.filter(i => i.completed);
      case IN_PROGRESS:
        return items.filter(i => !i.completed);
      default:
        return items;
    }
  }
);

