import {
  uniqueId
} from 'lodash';

import {
  ADD_ITEM,
  REMOVE_ITEM,
  TOGGLE_ITEM,
  SET_FILTER,
  SHOW_ALL
} from './actions/todos';

const ITEM_PREFIX = 'items__';

export const initialState = {
  itemFilter: SHOW_ALL,
  items: [
    { id: uniqueId(ITEM_PREFIX), content: 'Call mum', completed: false},
    { id: uniqueId(ITEM_PREFIX), content: 'Buy cat food', completed: false},
    { id: uniqueId(ITEM_PREFIX), content: 'Water the plants', completed: false},
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const newItem = {
        id: uniqueId(ITEM_PREFIX),
        content: action.content,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };


    case REMOVE_ITEM:
      return {
        ...state,
        items: state.items.filter(i => i.id !== action.id)
      }

    case TOGGLE_ITEM:
      return {
        ...state,
        items: state.items.map(item => {
          if (item.id === action.id) {
            item.completed = !item.completed
          }

          return item;
        })
      };


    case SET_FILTER: 
      return {
        ...state,
        itemFilter: action.itemFilter,
      };

    default:
      return state;
  }
};

export default reducer;
