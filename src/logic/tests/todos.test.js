import reducer, { 
  initialState, 
} from '../todos';
import {
  addItem,
  removeItem,
  toggleItem,
  setItemFilter
} from '../actions/todos.js';

describe('reducer', () => {
  it('returns state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('uses initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('adds new items on ADD_ITEM', () => {
    const state = {
      items: [
        { id: 1, content: 'first' },
        { id: 2, content: 'second' },
      ]
    }
    const mockAction = addItem('third');
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id).toContain('items__');
    expect(result.items[2].content).toEqual('third');
  });

  it('removes items on REMOVE_ITEM', () => {
    const state = {
      items: [
        { id: 1, content: 'first' },
        { id: 2, content: 'second' },
      ]
    };

    const mockRemove = removeItem(1);
    const result = reducer(state, mockRemove);
    
    expect(result.items.length).toBe(1);
    expect(result.items[0].content).toBe('second');
  });

  describe('#TOGGLE_ITEM', () => {
    it('sets completed status to true', () => {
      const state = {
        items: [
          { id: 1, content: 'first', completed: false },
          { id: 2, content: 'second', completed: false},
        ]
      };

      const mockToggle = toggleItem(1);
      const result = reducer(state, mockToggle);

      expect(result.items[0].completed).toBe(true);
    });

    it('calling it twice resets the completed status', () => {
      const state = {
        items: [
          { id: 1, content: 'first', completed: false },
          { id: 2, content: 'second', completed: false},
        ]
      };

      const mockToggle = toggleItem(1);
      const result = reducer(state, mockToggle);

      expect(result.items[0].completed).toBe(true);
      const secondResult = reducer(state, mockToggle);
      expect(secondResult.items[0].completed).toBe(false);
    });

  });


  describe('#SET_FILTER', () => {
    it('sets the new filter', () => {
      const state = {
        itemFilter: 'SHOW_ALL'
      };

      const mockSetFilter = setItemFilter('YOU_KNOW_NOTHING');
      const result = reducer(state, mockSetFilter);
      expect(result.itemFilter).toEqual('YOU_KNOW_NOTHING');
    });
  });
  
});
