import React from 'react';
import { shallow } from 'enzyme';
import { ItemsList } from '../index';

const defaultProps = {
  items: [],
};

describe('ItemsList', () => {
  it('renders without crashing', () => {
    shallow(<ItemsList {...defaultProps} />);
  });

  it('display warning message if no items', () => {
    const renderedItem = shallow(<ItemsList {...defaultProps} items={[]} />);
    expect(renderedItem.find('.items-missing')).toHaveLength(1);
  });

  it('doesn not display warning message if items are present', () => {
    const items = [{ id: 1, content: 'Test 1' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('.items-missing')).toHaveLength(0);
  });

  it('renders items as list items', () => {
    const items = [{ id: 1, content: 'Test 1' }, { id: 2, content: 'Test 2' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('li')).toHaveLength(2);
  });

  it('shows a delete button', () => {
    const items = [{ id: 1, content: 'Test 1' }, { id: 2, content: 'Test 2' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('.item-remove-button')).toHaveLength(2);
  });

  it('calls the provided function when clicked', () => {
    const removeItemSpy = jest.fn();
    const items = [{ id: 1, content: 'Test 1'}];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} removeItem={removeItemSpy} />);

    renderedItem.find('.item-remove-button').first().simulate('click');
    expect(removeItemSpy).toHaveBeenCalledWith(1);
  }); 

  it('displays a checkbox for completing items', () => {
    const items = [{ id: 1, content: 'Test 1' }, { id: 2, content: 'Test 2' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('.item-complete-toggle')).toHaveLength(2);
  });

  it('shows the checkbox checked when an item is completed', () => {
    const items = [{id: 1, content: 'Test 1', completed: true}, {id: 2, content: 'Test 2', completed: false}];
   const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);

   expect(renderedItem.find('.item-complete-toggle').first().prop('checked')).toBe(true);
   expect(renderedItem.find('.item-complete-toggle').last().prop('checked')).toBe(false);
  });

  it('calls the toggle function when clicked', () => {
    const toggleItemSpy = jest.fn();
    const items = [{ id: 1, content: 'Test 1' }, { id: 2, content: 'Test 2' }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} toggleItem={toggleItemSpy} />);
    renderedItem.find('.item-complete-toggle').first().simulate('click');
    expect(toggleItemSpy).toHaveBeenCalledWith(1);
  });
});
