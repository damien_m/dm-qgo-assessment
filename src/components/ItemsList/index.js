import React from 'react';
import {
  array,
  func
} from 'prop-types';
import { connect } from 'react-redux';
import './styles.css';
import {
  removeItem,
  toggleItem
} from '../../logic/actions/todos';
import { getVisibleItems } from '../../logic/selectors/todos';

export const ItemsList = ({ items, removeItem, toggleItem }) => {
  return (
    <div>
      <ul className="itemsList-ul">
        {items.length < 1 && 
          <p className="items-missing">Add some tasks above.</p>
        }
        {/*TODO Make this a component*/}
        {items.map(item => (
          <li key={item.id}>
            <div className="item-content">
              {item.content}
            </div>

            <div className="item-actions-wrapper">
              <label className="item-toggle-wrapper">
                <input
                  type="checkbox" 
                  className="item-complete-toggle" 
                  onClick={() => {toggleItem(item.id)}}
                  checked={item.completed}
                  />
              </label>

              {/*TODO investigate better handling of inline function usage */}
              <button 
                onClick={() => { removeItem(item.id) }} 
                className="item-remove-button">
              X   
              </button>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

ItemsList.propTypes = {
  items: array.isRequired,
  removeItem: func,
  toggleItem: func
};

ItemsList.defaultProps = {
  removeItem: () => {},
  toggleItem: () => {}
};

const mapStateToProps = state => ({
  items: getVisibleItems(state.todos)
});

const mapDispatchToProps = dispatch => ({
  removeItem(id) {
    dispatch(removeItem(id));
  },
  toggleItem(id) {
    dispatch(toggleItem(id))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
