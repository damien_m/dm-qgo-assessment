# Damien McMahon Sky Assessment

## Approach
Thanks for making a test that's a realistic assessment of what we as Frontenders do. I've been made to traverse binary trees when I want to be showing how I love to build UIs that are composable and follow a Data-Down Actions Up approach.

I love using React, trying to make use of functional stateless components where possible and like to leverage libraries such as recompose to get as far as possible with that.

I've used reselect in this test, adding it when I felt it was needed (look at the git history for proof). I chose this even though for such a simple project it's overkill to be worrying about memoising selectors but I wanted to show that I'm aware (if not massively experienced in using it).

I'll be honest that I'm not always a TDDer, mostly through sheer impatience but I *do* love to have tests. I'm a fan of unit tests for complex logic and lots of E2E (integration/acceptance) testing to cover actual usage.

## Improvements
- Complete the Filter Selector
I ran out of time to implement the UI and add tests for the filter selector.

- CSS
I love CSS (and particularly SASS) as much as JS. I'm a Frontender with a love of design and code and so it goes without saying that this UI isn't my best by any stretch. I'd implement a BEM led SASS architecture. I've looked at StyledComponents and can't quite mae my mind up.

- More Components
The ItemList should probably have an Item component that gets mapped.

- DRYer tests
I'm not a fan of DRYing code just for the sake of it but I'd clean up duplicated code where possible.

- Smaller Files
I've started to break things down a bit but I'm not happy with Components also connecting to the store. I'd have a containers directory as I don't think Components should know about being connected to stores.

- General Code Quality
I'm a fan of readable code, for other devs and my future self. I may have used code that's trying to do too much in one line. I'd probably break this down.

- Aspects of Functional Code
I'm a fan of immutability, purity, fewer glue variables and using functional techniques to make code more descriptive.


